import { Player } from './player.model';
import { Transaction } from './transaction.model';

export interface TransactionPlayer {
  transaction: Transaction;
  player: Player;
}
