import { Player } from './player.model';
import { Ticket } from './ticket.model';

export interface TicketPlayer {
  ticket: Ticket;
  player: Player;
}
