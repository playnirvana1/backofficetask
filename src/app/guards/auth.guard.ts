import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { RoutePaths } from '../enums/routes';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(): Observable<boolean> {
    return this.userService.isAuthorized().pipe(
      map((isAuthorized: boolean) => {
        if (isAuthorized) {
          return true;
        } else {
          this.router.navigate([RoutePaths.LOGIN]);
          return false;
        }
      })
    );
  }
}
