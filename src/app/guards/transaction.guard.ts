import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Grant } from '../models/user.model';
import { UserService } from '../services/user.service';
import { RoutePaths } from '../enums/routes';

@Injectable({
  providedIn: 'root',
})
export class TransactionGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) {}

  canActivate(): boolean {
    if (
      this.userService
        .getCurrentUser()
        .grants.includes(Grant.CanViewTransactions)
    ) {
      return true;
    } else {
      this.router.navigate([RoutePaths.DASHBOARD]);
      return false;
    }
  }
}
