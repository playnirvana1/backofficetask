export enum RoutePaths {
  LOGIN = '',
  DASHBOARD = 'dashboard',
  TRANSACTIONS = 'transactions',
  TRANSACTION_DETAIL = 'transactions/:id',
  TICKETS = 'tickets',
  TICKET_DETAIL = 'tickets/:id'
}
