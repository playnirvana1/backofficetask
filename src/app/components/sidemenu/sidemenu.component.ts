import { Component, EventEmitter, Output } from '@angular/core';
import { RoutePaths } from 'src/app/enums/routes';
import { Grant } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent {
  @Output() menuItemClicked: EventEmitter<any> = new EventEmitter();

  get RoutePaths() {
    return RoutePaths;
  }

  get showTransaction() {
    const user = this.userService.getCurrentUser();
    return user ? user.grants.includes(Grant.CanViewTransactions) : false;
  }

  get showTicket() {
    const user = this.userService.getCurrentUser();
    return user ? user.grants.includes(Grant.CanViewTickets) : false;
  }

  constructor(private userService: UserService) {}

  itemClicked() {
    this.menuItemClicked.emit();
  }
}
