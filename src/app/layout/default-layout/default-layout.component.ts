import { Component, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { RoutePaths } from 'src/app/enums/routes';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-default-layout',
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.scss'],
})
export class DefaultLayoutComponent {
  @ViewChild('drawer') drawer!: MatDrawer;
  private initials: string;
  private readonly smallScreenWidth = 300;

  get routes() {
    return RoutePaths;
  }

  get nameLetters() {
    return this.initials;
  }

  constructor(private router: Router, private userService: UserService, private translate: TranslateService) {
    this.initials = this.getInitials();
  }

  logout() {
    this.userService.logout().subscribe({
      next: () => {
        this.router.navigate([RoutePaths.LOGIN]);
      }
    });
  }

  switchLanguage() {
    this.translate.currentLang === 'en' ? this.translate.use('hr') : this.translate.use('en');
  }

  redirect(route: RoutePaths) {
    this.router.navigate([route]);
  }

  //closing sidenav on item click, only for small screens
  onItemClick() {
    console.log('item clicked ', window.innerWidth);

    if (window.innerWidth <= this.smallScreenWidth) {
      this.drawer.close();
    }
  }

  getInitials(): string {
    const user = this.userService.getCurrentUser();
    return user ? user.username.charAt(0) : '';
  }
}
