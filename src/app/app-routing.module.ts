import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutePaths } from './enums/routes';
import { LoginComponent } from './pages/login/login.component';
import { DefaultLayoutComponent } from './layout/default-layout/default-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { TransactionsComponent } from './pages/transactions/transactions.component';
import { TicketsComponent } from './pages/tickets/tickets.component';
import { AuthGuard } from './guards/auth.guard';
import { TransactionGuard } from './guards/transaction.guard';
import { TicketGuard } from './guards/ticket.guard';
import { TransactionDetailComponent } from './pages/transactions/transaction-detail/transaction-detail.component';
import { TicketDetailComponent } from './pages/tickets/ticket-detail/ticket-detail.component';

const routes: Routes = [
  {
    path: RoutePaths.LOGIN,
    component: LoginComponent,
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: RoutePaths.DASHBOARD, pathMatch: 'full' },
      {
        path: RoutePaths.DASHBOARD,
        component: DashboardComponent,
        pathMatch: 'full',
      },
      {
        path: RoutePaths.TRANSACTIONS,
        component: TransactionsComponent,
        pathMatch: 'full',
        canActivate: [TransactionGuard],
      },
      {
        path: RoutePaths.TRANSACTION_DETAIL,
        component: TransactionDetailComponent,
        pathMatch: 'full',
        canActivate: [TransactionGuard],
      },
      {
        path: RoutePaths.TICKETS,
        component: TicketsComponent,
        pathMatch: 'full',
        canActivate: [TicketGuard],
      },
      {
        path: RoutePaths.TICKET_DETAIL,
        component: TicketDetailComponent,
        pathMatch: 'full',
        canActivate: [TicketGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
