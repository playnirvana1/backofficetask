import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { RoutePaths } from 'src/app/enums/routes';
import { Player } from 'src/app/models/player.model';
import { TicketStatus } from 'src/app/models/ticket.model';
import { TransactionPlayer } from 'src/app/models/transaction-player.model';
import {
  Transaction,
  TransactionDirection,
  TransactionProvider,
  TransactionType,
} from 'src/app/models/transaction.model';
import { PlayerService } from 'src/app/services/player.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss'],
})
export class TransactionsComponent implements OnInit {
  displayedColumns: string[] = [
    'id',
    'player',
    'externalId',
    'type',
    'provider',
    'direction',
    'created',
    'amount',
  ];

  public filterForm = new FormGroup({
    playerId: new FormControl<string>(undefined),
    externalId: new FormControl<string>(undefined),
    type: new FormControl<TransactionType>(undefined),
    provider: new FormControl<TransactionProvider>(undefined),
    direction: new FormControl<TransactionDirection>(undefined),
    createdFrom: new FormControl<string>(undefined),
    createdTo: new FormControl<string>(undefined),
  });

  public tableData?: TransactionPlayer[];

  public allPlayers: Player[];

  get allTransactionTypes() {
    return Object.values(TransactionType);
  }

  get allTransactionProviders() {
    return Object.values(TransactionProvider);
  }

  get allTransactionDirections() {
    return Object.values(TransactionDirection);
  }

  constructor(
    private transactionService: TransactionService,
    private playerService: PlayerService,
    private router: Router
  ) {
    this.getTransactions();
  }

  ngOnInit(): void {}

  private getTransactions() {
    this.tableData = undefined;
    forkJoin([
      this.transactionService.getTransactions({
        playerId: this.filterForm.value.playerId,
        externalId: this.filterForm.value.externalId,
        type: this.filterForm.value.type,
        provider: this.filterForm.value.provider,
        direction: this.filterForm.value.direction,
        createdFrom: this.filterForm.value.createdFrom,
        createdTo: this.filterForm.value.createdTo,
      }),
      this.playerService.getPlayers(),
    ]).subscribe((res) => {
      const transactionRes = res[0];
      const playerRes = res[1];

      this.allPlayers = playerRes;
      let result: TransactionPlayer[] = [];
      transactionRes.forEach((item) => {
        const player = playerRes.find((player) => player.id === item.playerId);
        result.push({
          transaction: item,
          player: player,
        });
      });

      this.tableData = result;
    });
  }

  onRowClick(transaction: Transaction) {
    this.router.navigate([RoutePaths.TRANSACTIONS, transaction.id]);
  }

  applyFilters() {
    this.getTransactions();
  }
  clearFilters() {
    this.filterForm.reset();
    this.getTransactions();
  }
}
