import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RoutePaths } from 'src/app/enums/routes';
import { Transaction } from 'src/app/models/transaction.model';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-transaction-detail',
  templateUrl: './transaction-detail.component.html',
  styleUrls: ['./transaction-detail.component.scss'],
})
export class TransactionDetailComponent implements OnInit {
  @Input() id = '';

  public transaction: Transaction;

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private transactionService: TransactionService
  ) {}

  ngOnInit(): void {
    //Whole part with ActivatedRoute is not necessary in newer versions of angular, having an input
    //with the right naming maps value to the input
    this.id = this.activeRoute.snapshot.params['id'];
    if (this.id === '') {
      this.router.navigate([RoutePaths.TRANSACTIONS]);
    }

    this.transactionService.getTransaction(this.id).subscribe({
      next: (res) => {
        this.transaction = res;
      },
    });
  }

  back() {
    this.router.navigate([RoutePaths.TRANSACTIONS]);
  }
}
