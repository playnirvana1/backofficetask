import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { RoutePaths } from 'src/app/enums/routes';
import { Player } from 'src/app/models/player.model';
import { TicketPlayer } from 'src/app/models/ticket-player';
import { Ticket, TicketStatus } from 'src/app/models/ticket.model';
import { PlayerService } from 'src/app/services/player.service';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.scss'],
})
export class TicketsComponent implements OnInit {
  displayedColumns: string[] = [
    'playerId',
    'status',
    'currency',
    'numOfBets',
    'created',
  ];

  public tableData?: TicketPlayer[];
  public allPlayers: Player[];

  public filterForm = new FormGroup({
    playerId: new FormControl<string>(undefined),
    status: new FormControl<TicketStatus>(undefined),
    createdFrom: new FormControl<string>(undefined),
    createdTo: new FormControl<string>(undefined),
  });

  get allTicketStatus(): string[] {
    return Object.values(TicketStatus);
  }

  constructor(
    private playerService: PlayerService,
    private ticketService: TicketService,
    private router: Router
  ) {
    this.getTickets();
  }

  ngOnInit(): void {}

  private getTickets() {
    this.tableData = undefined;
    forkJoin([
      this.ticketService.getTickets({
        playerId: this.filterForm.value.playerId,
        status: this.filterForm.value.status,
        createdFrom: this.filterForm.value.createdFrom,
        createdTo: this.filterForm.value.createdTo,
      }),
      this.playerService.getPlayers(),
    ]).subscribe((res) => {
      const ticketRes = res[0];
      const playerRes = res[1];

      this.allPlayers = playerRes;
      let result: TicketPlayer[] = [];
      ticketRes.forEach((item) => {
        const player = playerRes.find((player) => player.id === item.playerId);
        result.push({
          ticket: item,
          player: player,
        });
      });

      this.tableData = result;
    });
  }

  onRowClick(ticket: Ticket) {
    this.router.navigate([RoutePaths.TICKETS, ticket.id]);
  }

  applyFilters() {
    this.getTickets();
  }
  clearFilters() {
    this.filterForm.reset();
    this.getTickets();
  }
}
