import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { RoutePaths } from 'src/app/enums/routes';
import { Ticket } from 'src/app/models/ticket.model';
import { Transaction } from 'src/app/models/transaction.model';
import { TicketService } from 'src/app/services/ticket.service';
import { TransactionService } from 'src/app/services/transaction.service';

@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.scss'],
})
export class TicketDetailComponent implements OnInit {
  @Input() id = '';

  public ticket: Ticket;
  public linkedTransactions?: Transaction[];

  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private ticketService: TicketService,
    private transactionService: TransactionService
  ) {}

  ngOnInit(): void {
    this.id = this.activeRoute.snapshot.params['id'];
    if (this.id === '') {
      this.router.navigate([RoutePaths.TICKETS]);
    }

    this.ticketService.getTicket(this.id).subscribe({
      next: (res) => {
        this.ticket = res;
        this.getLinkedTransactions(res.id);
      },
    });
  }

  getLinkedTransactions(ticketId: string) {
    this.transactionService
      .getTransactions({ externalId: ticketId })
      .subscribe({
        next: (res) => {
          this.linkedTransactions = res;
        },
      });
  }

  back() {
    this.router.navigate([RoutePaths.TICKETS]);
  }
}
