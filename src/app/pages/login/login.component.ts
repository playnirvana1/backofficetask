import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutePaths } from 'src/app/enums/routes';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup<any> = new FormGroup({
    username: new FormControl<string>('', Validators.required),
    password: new FormControl<string>('', Validators.required),
  });

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit(): void {
    if (this.userService.isAuthorized)
      this.router.navigate([RoutePaths.DASHBOARD]);
  }

  login() {
    console.log(this.loginForm.value.username, this.loginForm.value.password);
    this.userService
      .login(this.loginForm.value.username, this.loginForm.value.password)
      .subscribe({
        next: (res) => {
          this.router.navigate([RoutePaths.DASHBOARD]);
        },
        error: () => {
          alert('Wrong credentials');
        },
      });
  }
}
