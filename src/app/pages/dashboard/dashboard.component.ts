import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RoutePaths } from 'src/app/enums/routes';
import { TransactionProvider } from 'src/app/models/transaction.model';
import { Grant, User } from 'src/app/models/user.model';
import { TicketService } from 'src/app/services/ticket.service';
import { TransactionService } from 'src/app/services/transaction.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  get paths() {
    return RoutePaths;
  }

  get grant() {
    return Grant;
  }

  user: User;

  constructor(private router: Router, userService: UserService) {
    this.user = userService.getCurrentUser();
  }

  ngOnInit(): void {}

  navigate(path: RoutePaths) {
    this.router.navigate([path]);
  }
}
